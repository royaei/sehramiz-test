@extends('admin.layout.main')

@section('title', 'کاهش اعتبار')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">کاهش اعتبار
            @include('admin.partner.navigation')
        </h1>
        <div class="panel panel-default">
            <div class="panel-heading">کاهش اعتبار برای {{$partner->name}}
                <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\PartnerController@getView', $partner->partner_id)}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                <form action="{{action('Admin\PartnerCreditController@postDecrease')}}" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>مقدار کاهش</label>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                {!! Form::text('credit', '', ['class' => 'form-control en ltr p-sep-ltr']) !!}
                                <div class="input-group-addon">ریال</div>
                            </div>
                            {!! $errors->first('credit', "<p class='text text-danger'>:message</p>") !!}
                            <p class="text-warning">حداکثر مبلغ عودت: {{price($partnerCreditHolder->credit())->sep()->fa()}} ریال</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>توضیحات</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::textarea('description', '', ['class' => 'form-control', 'rows' => 3]) !!}
                            {!! $errors->first('description', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="partner_id" value="{{$partner->partner_id}}">
                        <a class="btn btn-default pull-right" href="{{action('Admin\PartnerController@getView', $partner->partner_id)}}">برگشت</a>
                        <button type="submit" class="btn btn-warning pull-left"><i class="fa fa-minus"></i> کاهش</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

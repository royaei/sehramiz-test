{{-- C::PC_E_INCREASE_ADMIN --}}
<table class="table table-bordered">
    <tbody>
        <tr>
            <th class="active">ادمین</th>
            <td><a href="{{action('Admin\AdminController@getUpdate', $creditHolder->event()->adminId())}}">{{$creditHolder->event()->adminName()}}</a></td>
        </tr>
        <tr>
            <th class="active">توضیحات</th>
            <td>{{$creditHolder->event()->description()}}</td>
        </tr>
    </tbody>
</table>

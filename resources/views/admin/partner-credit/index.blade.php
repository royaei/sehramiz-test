@extends('admin.layout.main')

@section('title', 'اعتبارات همکاران')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">اعتبارات همکاران</h1>
        <div class="panel panel-default">
            <div class="panel-heading">اعتبارات همکاران</div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-10 col-sm-10 col-xs-10">
                                فیلتر
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2 text-left">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <?php
                                            $requests = Request::all();
                                        ?>
                                        <a href="{{action('Admin\PartnerCreditController@getPrevPeriod')}}?{{http_build_query($requests)}}" class="btn btn-default btn-xs" title="بازه قبلی"><i class="fa fa-arrow-right"></i></a>
                                        <a href="{{action('Admin\PartnerCreditController@getNextPeriod')}}?{{http_build_query($requests)}}" class="btn btn-default btn-xs" title="بازه بعدی"><i class="fa fa-arrow-left"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="" method="get">
                            <div class="col-md-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>از تاریخ:</label>
                                        {!! Form::text('from_date', $fromDate->toj()->fa('Y/m/d'), ['class' => 'form-control date-picker']) !!}
                                        {!! $errors->first('from_date', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>نوع:</label>
                                        {!! Form::select('code', Helper::partnerCreditEventCodesList(), $code, ['placeholder' => 'همه', 'class' => 'form-control']) !!}
                                        {!! $errors->first('code', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                                <br>
                                @if (Request::has('partner_id'))
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label>همکار:</label>
                                            <span class="label label-info">{{$partner->name}}</span>
                                            <input type="hidden" name="partner_id" value="{{$partnerId}}">
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>تا تاریخ:</label>
                                        {!! Form::text('to_date', $toDate->toj()->fa('Y/m/d'), ['class' => 'form-control date-picker']) !!}
                                        {!! $errors->first('to_date', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                                <br>
                                <div class="form-inline">
                                    <div class="form-group">
                                        <label>وضعیت پرداخت</label>
                                        {!! Form::select('payment', Helper::paymentsList(false), $payment, ['placeholder' => 'همه', 'class' => 'form-control']) !!}
                                        {!! $errors->first('payment', "<p class='text text-danger'>:message</p>") !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-info pull-left"><i class="fa fa-refresh"></i> بروزرسانی</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <table class="table table-bordered table-hover custom-datatable">
                    <thead>
                        <tr>
                            <th class="en">#</th>
                            <th>همکار</th>
                            <th>نوع عملیات</th>
                            <th>تاریخ</th>
                            <th>مبلغ</th>
                            <th>پرداخت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($creditEvents as $event)
                        <tr>
                            <td class="en">{{$event->partner_credit_event_id}}</td>
                            <td><a href="{{action('Admin\PartnerController@getView', $event->partner_id)}}">{{$event->partner_name}}</a></td>
                            <td>{{$event->code_message}}</td>
                            <td class="ltr text-right">{{$event->created_at_jalali}}</td>
                            <td><span class="en" dir="ltr">{{price($event->price)->sep()}}</span> ریال</td>
                            <td><span class="label label-{{$event->payment_status_code}}">{{$event->payment_status_message}}</span></td>
                            <td>
                                <a href="{{action('Admin\PartnerCreditController@getView', $event->partner_credit_event_id)}}" class="btn btn-primary btn-xs" title="مشاهده"><i class="fa fa-eye"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script>
    $('.custom-datatable').DataTable( {
        responsive: true,
        language: window.datatableLanguage,
        order: [[ 3, "desc" ]]
    });
</script>
@stop

@extends('admin.layout.main')

@section('title', 'ویرایش جشنواره')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">جشنواره‌ها</h1>
        <div class="panel panel-default">
            <div class="panel-heading">ویرایش جشنواره
                <a class="btn btn-default btn-xs pull-left" href="{{action('Admin\FestivalController@getIndex')}}" title="برگشت"><i class="fa fa-reply"></i></a>
            </div>
            <div class="panel-body">
                <form action="{{action('Admin\FestivalController@postUpdate')}}" method="post">
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>عنوان جشنواره @required</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('name', $festival->name, ['class' => 'form-control', 'required']) !!}
                            {!! $errors->first('name', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>کد جشنواره @required</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::text('codename', $festival->codename, ['class' => 'form-control en ltr', 'required']) !!}
                            {!! $errors->first('codename', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>مبلغ هر کد @required</label>
                        </div>
                        <div class="col-md-3">
                            <div class="input-group">
                                {!! Form::text('code_price', $festival->code_price, ['class' => 'form-control en ltr p-sep-ltr']) !!}
                                <div class="input-group-addon">ریال</div>
                            </div>
                            {!! $errors->first('code_price', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>حداکثر تعداد کدها @required</label>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                {!! Form::number('code_count_max', $festival->code_count_max, ['class' => 'en ltr form-control clear-addon']) !!}
                                <div class="input-group-addon radio">
                                    <?php
                                        $codeCountMax = old('code_count_max') ?: $festival->code_count_max;
                                    ?>
                                    <label><input class="clear-addon-parent" {{empty($codeCountMax) ? 'checked' : ''}} type="radio"> نامحدود</label>
                                </div>
                            </div>
                            {!! $errors->first('code_count_max', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>همکاران جشنواره</label>
                        </div>
                        <div class="col-md-6">
                            {!! Form::select('partners[]', $partners, $selectedPartners, ['class' => 'form-control multiselect', 'multiple' => 'multiple']) !!}
                            {!! $errors->first('partners', "<p class='text text-danger'>:message</p>") !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <label>وضعیت</label>
                        </div>
                        <div class="col-md-2">
                            <label>غیر فعال</label>
                            {!! Form::radio('status', '0', $festival->status == 0 ? true : false, ['class' => 'icheck-status disable']) !!}
                            <label>فعال</label>
                            {!! Form::radio('status', '1', $festival->status == 1 ? true : false, ['class' => 'icheck-status enable']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="festival_id" value="{{$festival->festival_id}}">
                        <div class="col-md-6 text-right">
                            <a href="{{action('Admin\FestivalController@getIndex')}}" class="btn btn-default">برگشت</a>
                        </div>
                        <div class="col-md-6 text-left">
                            <button type="submit" class="btn btn-info"><i class="fa fa-pencil"></i> ذخیره</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script>
    $(function() {
        $('.clear-addon-parent').change(function() {
            if ($(this).is(':checked')) {
                $(this).closest('.input-group').find('.clear-addon').val('');
            }
        });
        $('.clear-addon').keyup(function() {
            if ($(this).val().length > 0) {
                $(this).next('.input-group-addon').find('.clear-addon-parent').prop('checked', false);
            }
        });
        $('.multiselect').multiSelect({
            selectableHeader: "<div class='custom-header'>موجود</div>",
            selectionHeader: "<div class='custom-header'>انتخاب شده</div>",
        });
    });
</script>
@stop

@extends('admin.layout.main')

@section('title', 'لیست کاربران')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">کاربران</h1>
        <div class="panel panel-default">
            <div class="panel-heading">لیست کاربران</div>
            <div class="panel-body">
                @if (Session::has('f-message'))
                    <div class="alert alert-{{Session::get('f-message')['t']}}">
                        {!! Session::get('f-message')['m'] !!}
                    </div>
                @endif

                <table class="table table-bordered table-hover custom-datatable">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="pr">شماره همراه</th>
                            <th>نام و نام خانوادگی</th>
                            <th>وضعیت</th>
                            <th>عملیات</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
<script>
    $('.custom-datatable').DataTable( {
        processing: true,
        serverSide: true,
        ajax: window.BASE_URL + '/datatable/data/users',
        responsive: true,
        language: window.datatableLanguage,
        order: [[ 0, "desc" ]],
        columns: [
           { data: 'user_id', name: 'user_id', class: 'en' },
           { data: 'mobile', name: 'mobile', className: 'en' },
           { data: 'name', name: 'name' },
           { data: 'status', name: 'status' },
           { data: 'ops', name: 'ops' }
       ]
    });
</script>
@stop

@extends('general.print.main')

@section('title', 'معین حساب')

@section('content')
<h2 class="text-center">معین حساب</h2>
<table class="table clean">
    <tr>
        <td>طرف حساب: {{$pool->name}}</td>
        <td class="text-left">از تاریخ: {{@to_j($from->format(C::DATE_FORMAT), false)}}</td>
    </tr>
    <tr>
        <td></td>
        <td class="text-left">تا تاریخ: {{@to_j($to->format(C::DATE_FORMAT), false)}}</td>
    </tr>
</table>
<table class="table table-bordered table-hover custom-datatable">
    <thead>
        <tr>
            <th>ردیف</th>
            <th>تاریخ فاکتور</th>
            <th>شماره فاکتور</th>
            <th>شماره حواله</th>
            <th>بدهکاری</th>
            <th>بستانکاری</th>
            <th>مانده</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ledgers as $position => $ledger)
            <tr class="{{$ledger->isDraft() ? 'active' : ''}}">
                <td class="en">{{++$position}}</td>
                <td class="ltr text-right">{{to_jalali($ledger->date, 'Y/m/d H:i:s l')}}</td>
                <td class="en">
                    @if ($ledger->isFactor())
                    {{$ledger->factorId}}
                    @endif
                </td>
                <td class="en">
                    @if ($ledger->isDraft())
                    {{$ledger->draftId}}
                    @endif
                </td>
                <td><span class="en">{{price($ledger->liability)->sep()}}</span> ریال</td>
                <td><span class="en">{{price($ledger->credit)->sep()}}</span> ریال</td>
                <td><span class="en">{{price(abs($ledger->remaining))->sep()}}</span> ریال {{$ledger->remainingStatus}}</td>
            </tr>
        @endforeach
        @if ($ledgers->count() > 0)
        <tr class="success">
            <td class="en">{{++$position}}</td>
            <td colspan="3" class="text-left">جمع کل:</td>
            <td><span class="en">{{price($ledger->totalLiability)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($ledger->totalCredit)->sep()}}</span> ریال</td>
            <td><span class="en">{{price(abs($ledger->remaining))->sep()}}</span> ریال {{$ledger->remainingStatus}}</td>
        </tr>
        @endif
    </tbody>
</table>
@stop

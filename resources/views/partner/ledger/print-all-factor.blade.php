@extends('general.print.main')

@section('title', 'مشاهده فاکتور')

@section('content')
<h2 class="text-center">تمامی فاکتورها</h2>
<h3 class="text-center">از تاریخ {{to_j($from, false)}} تا تاریخ {{to_j($to, false)}}</h3>
<br>
@foreach ($ledgers as $ledger)
<div class="avoid-page-break-inside">
<h3 class="text-center">فاکتور</h3>
<table class="table clean">
    <tr>
        <td>طرف حساب: {{$pool->name}}</td>
        <td class="text-left">تاریخ فاکتور: <span dir="ltr">{{@to_jalali($ledger->date, 'Y/m/d H:i:s l')}}</span></td>
    </tr>
    <tr>
        <td>شماره فاکتور: {{@tr_num($ledger->factor_id, 'fa')}}</td>
        <td class="text-left">ساعت فاکتور: {{@to_time($ledger->date, false, true)}}</td>
    </tr>
    <tr>
        <td>نام سانس: {{$ledger->reservation->saens_name}}</td>
        <td class="text-left">جنسیت: {{Helper::printGender($ledger->reservation->gender)}}</td>
    </tr>
</table>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان</th>
            <th>تعداد</th>
            <th>قیمت</th>
            <th>پس از تخفیف</th>
            <th>جمع کل</th>
        </tr>
    </thead>
    <?php $i = 0 ?>
    <tbody>
        @if ($ledger->reservation->count_adult != '0')
        <tr>
            <td class="en">{{++$i}}</td>
            <td>بزرگسال</td>
            <td class="en">{{$ledger->reservation->count_adult}}</td>
            <td><span class="en">{{price($ledger->reservation->price_market_adult)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($ledger->reservation->price_buy_adult)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($ledger->reservation->count_adult * $ledger->reservation->price_buy_adult)->sep()}}</span> ریال</td>
        </tr>
        @endif
        @if ($ledger->reservation->count_child != '0')
        <tr>
            <td class="en">{{++$i}}</td>
            <td>خردسال</td>
            <td class="en">{{$ledger->reservation->count_child}}</td>
            <td><span class="en">{{price($ledger->reservation->price_market_child)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($ledger->reservation->price_buy_child)->sep()}}</span> ریال</td>
            <td><span class="en">{{price($ledger->reservation->count_child * $ledger->reservation->price_buy_child)->sep()}}</span> ریال</td>
        </tr>
        @endif
        <tr>
            <td class="en">{{++$i}}</td>
            <td colspan="3"></td>
            <td class="active">جمع کل:</td>
            <td class="success"><span class="en">{{price($ledger->reservation->pay_to_pool)->sep()}}</span> ریال</td>
        </tr>
    </tbody>
</table>
<hr>
</div>
@endforeach
@stop

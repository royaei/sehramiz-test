<?php

return [
    C::OPERATOR_UPDATE_PROFILE."_OP" => 'ویرایش پروفایل',
    C::OPERATOR_UPDATE_PROFILE => '',

    C::OPERATOR_LOGIN."_OP" => 'ورود',
    C::OPERATOR_LOGIN => '',

    C::OPERATOR_LOGOUT."_OP" => 'خروج',
    C::OPERATOR_LOGOUT => '',

    C::OPERATOR_ENTERED_RESERVATION."_OP" => 'ورود رزرو',
    C::OPERATOR_ENTERED_RESERVATION => 'رزرو با آی‌ دی:id را ورود زد.',

    C::OPERATOR_CONNECTED."_OP" => 'اتصال اپراتور',
    C::OPERATOR_CONNECTED => '',
];

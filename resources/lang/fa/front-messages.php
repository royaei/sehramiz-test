<?php

return [
    'contact_sent_successfully' => 'اطلاعات با موفقیت ارسال شد. متشکریم.',
    'profile_update_successfully' => 'پروفایل شما با موفقیت ویرایش شد.',
    'error_exception' => 'متاسفانه خطایی رخ داده است. لطفا بعدا امتحان کنید.',
    'error_refresh' => 'متاسفانه خطایی رخ داده است. لطفا صفحه را رفرش کنید.',
    'low_credit' => 'متاسفانه میزان اعتبار شما کافی نیست. میتوانید از قسمت <a href="'.action('Front\ProfileController@getCredit').'">پروفایل</a> اعتبار خود را افزایش دهید<br>و یا با استفاده از درگاه اینترنتی پرداخت را انجام دهید.',
    'select_port' => 'لطفا درگاه را انتخاب کنید.',
    'select_ticket' => 'لطفا حداقل یک بلیت انتخاب کنید.',
    'overflow_ticket' => 'تعداد بلیت انتخاب شده از ظرفیت باقیمانده بیشتر است.',
    'port_error' => 'درگاه با خطا مواجه شده است. لطفا بعدا دوباره امتحان فرمایید.',
    'time_length_with_minutes' => 'به مدت :hours ساعت و :minutes دقیقه',
    'time_length' => 'به مدت :hours ساعت',
    'time_period' => ':start الی :finish',
    'save_comment' => 'نظر شما با موفقیت ثبت شد و پس از تایید نمایش داده میشود.',
    'cancel_request_successfully' => 'درخواست شما با موفقیت ثبت شد.',

    'resend_email_verification' => 'ایمیل تایید به آدرس :email با موفقیت ارسال شد.',
    'resend_email_failed' => 'متاسفانه در ارسال ایمیل فعالسازی مشکلی پیش آمده است، لطفا بعدا دوباره امتحان نمایید.',
    'email_activated_before' => 'ایمیل شما قبلا فعال شده است.',
    'too_many_email_verification_sent' => 'شما تعداد زیادی درخواست ارسال ایمیل داشته‌اید. لطفا :available ثانیه دیگر دوباره امتحان کنید.',
    'email_verification_expired' => 'متاسفانه این کد منقضی شده است، لطفا دوباره اقدام به ارسال ایمیل فعالسازی کنید.',

    'resend_sms_verification' => 'پیامی حاوی کد فعالسازی به شماره :mobile ارسال شد.',
    'mobile_activated_before' => 'شماره همراه شما قبلا فعال شده است.',
    'too_many_sms_verification_sent' => 'شما تعداد زیادی درخواست ارسال پیام فعالسازی داشته‌اید. لطفا :available ثانیه دیگر دوباره امتحان کنید.',
    'sms_verification_expired' => 'متاسفانه این کد منقضی شده است، لطفا دوباره اقدام به ارسال پیام فعالسازی کنید.',
    'mobile_approved' => 'شماره همراه شما با موفقیت تایید شد.',

    'resend_email_newsletter_verification_subject' => 'فعالسازی خبرنامه - پول تیکت',

    'newsletter_profile_error' => 'برای عضویت در خبرنامه ابتدا باید استان، شهر و ایمیل خود را مقداردهی کنید.',

    'coupon_code_not_found' => 'کوپنی با این کد پیدا نشد.',
    'coupon_expired' => 'این کد منقضی شده است.',
    'coupon_only_in_buy_time' => 'این کوپن فقط در زمان خرید قابل استفاده است.',
    'too_many_coupon_entered' => 'شما تعداد زیادی امتحان کوپن داشته‌اید. لطفا :available ثانیه دیگر دوباره امتحان کنید.',
];

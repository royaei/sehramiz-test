<?php

return [
    C::FRONT_REGISTER."_OP" => 'ثبت نام کاربر',
    C::FRONT_REGISTER => 'کاربر با آی‌دی :id و نام :name ثبت‌ نام کرد.',

    C::FRONT_ADMIN_SMS_SEND."_OP" => 'ارسال اس ام اس از پنل ادمین',
    C::FRONT_ADMIN_SMS_SEND => '',

    C::FRONT_RESERV."_OP" => 'رزرو بلیت',
    C::FRONT_RESERV => 'کاربر با آی‌دی :id از استخر :pool_name رزرو با آی‌دی :id_reserv را انجام داد.',

    C::FRONT_SMS_VERIFICATION_SENT."_OP" => 'ارسال اس ام اس تایید همراه',
    C::FRONT_SMS_VERIFICATION_SENT => 'اس ام اس تایید همراه به کاربر با آی‌دی :id و نام :name ارسال شد.',

    C::FRONT_MOBILE_APPROVED."_OP" => 'موبایل تایید شد',
    C::FRONT_MOBILE_APPROVED => 'شماره همراه کاربر با آی‌دی :id و نام :name تایید شد.',

    C::FRONT_EMAIL_APPROVED."_OP" => 'ایمیل تایید شد',
    C::FRONT_EMAIL_APPROVED => 'ایمیل کاربر با آی‌دی :id و نام :name تایید شد.',

    C::FRONT_EMAIL_VERIFICATION_SENT."_OP" => 'ارسال ایمیل تایید ایمیل',
    C::FRONT_EMAIL_VERIFICATION_SENT => 'ایمیل تایید ایمیل به کاربر با آی‌دی :id و نام :name ارسال شد.',

    C::FRONT_NEWSLETTER_SUBSCRIBE."_OP" => 'عضویت در خبرنامه',
    C::FRONT_NEWSLETTER_SUBSCRIBE => 'کاربر با ایمیل :email در خبرنامه عضو شد.',

    C::FRONT_NEWSLETTER_APPROVED."_OP" => 'تایید عضویت در خبرنامه',
    C::FRONT_NEWSLETTER_APPROVED => 'خبرنامه کاربر با آی‌دی :id تایید شد.',

    C::FRONT_FORGOTTEN_PASSWORD_VIA_EMAIL."_OP" => 'فراموشی رمز بوسیله ایمیل',
    C::FRONT_FORGOTTEN_PASSWORD_VIA_EMAIL => 'ایمیل فراموشی رمز عبور به آدرس :email ارسال شد.',

    C::FRONT_FORGOTTEN_PASSWORD_VIA_MOBILE."_OP" => 'فراموشی رمز بوسیله همراه',
    C::FRONT_FORGOTTEN_PASSWORD_VIA_MOBILE => 'اس ام اس فراموشی رمز عبور به شماره :mobile ارسال شد.',

    C::FRONT_RESET_PASSWORD."_OP" => 'ریست پسورد',
    C::FRONT_RESET_PASSWORD => 'کاربر با آی‌دی :id و نام :name پسورد خود را ریست کرد.',

    C::FRONT_UPDATE_PROFILE."_OP" => 'ویرایش پروفایل',
    C::FRONT_UPDATE_PROFILE => 'کاربر با آی‌دی :id و نام :name پروفایل خود را ویرایش کرد.',

    C::FRONT_CONTACT_US_SEND."_OP" => 'ارسال تماس با ما',
    C::FRONT_CONTACT_US_SEND => 'کاربر با نام :name و ایمیل :email یک پیام ارسال کرد.',

    C::FRONT_POOL_COMMENT_SENT."_OP" => 'ارسال نظر در صفحه مجموعه',
    C::FRONT_POOL_COMMENT_SENT => 'کاربر با آی‌دی :id و نام :name در صفحه مجموعه با آی‌دی :pool_id نظر گذاشت.',

    C::FRONT_NEWSLETTER_UNSUBSCRIBE."_OP" => 'لغو عضویت در خبرنامه',
    C::FRONT_NEWSLETTER_UNSUBSCRIBE => 'کاربر با ایمیل :email از دریافت خبرنامه منصرف شد.',

    C::FRONT_INCREASE_CREDIT_START."_OP" => 'شروع افزایش اعتبار',
    C::FRONT_INCREASE_CREDIT_START => 'کاربر با آی‌دی :id افزایش اعتبار به ملبغ :price را شروع کرد.',

    C::FRONT_INCREASE_CREDIT_SUCCEED."_OP" => 'افزایش اعتبار',
    C::FRONT_INCREASE_CREDIT_SUCCEED => 'کاربر با آی‌دی :id افزایش اعتبار به مبلغ :price را انجام داد',
];

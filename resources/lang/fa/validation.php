<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => ':attribute با تایید آن یکسان نیست.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => ':attribute باید دارای :digits رقم باشد.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'email'                => 'لطفا ایمیل را به صورت صحیح وارد کنید.',
    'filled'               => 'The :attribute field is required.',
    'exists'               => ':attribute انتخاب شده، صحیح نیست.',
    'image'                => ':attribute باید یک تصویر باشد.',
    'in'                   => ':attribute انتخاب شده اشتباه است.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => ':attribute باید حداقل دارای :min کاراکتر باشد.',
        'numeric' => ':attribute باید حداقل :min باشد.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string' => ':attribute باید حداقل دارای :min کاراکتر باشد.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => ':attribute باید از نوع عدد باشد.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'لطفا مقدار فیلد :attribute را وارد کنید.',
    'required_if'          => 'مقداردهی فیلد :attribute زمانی که فیلد :other را، :value انتخاب میکنید، اجباری است.',
    'required_with'        => ':attribute زمانی که :values وجود دارند، باید وارد شود.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => ':attribute باید دارای :size کاراکتر باشد.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'این مقدار قبلا گرفته شده است.',
    'url'                  => ':attribute باید یک آدرس اینترنتی باشد.',
    'more_than'            => 'مقدار :attribute باید از :target_attribute بیشتر باشد.',
    'date_time'            => 'لطفا تاریخ و زمان را در فیلد :attribute به درستی وارد کنید.',
    'date'                 => 'لطفا تاریخ را در فیلد :attribute به درستی وارد کنید.',
    'time'                 => 'لطفا زمان را در فیلد :attribute به درستی وارد کنید',
    'token'                => 'انجام این عمل به دلایل امنیتی لغو گردید، لطفا دوباره امتحان نمایید.',
    'mobile'      => 'لطفا شماره موبایل را به درستی وارد نمایید.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name' => 'نام',
        'last_name' => 'نام خانوادگی',
        'mobile' => 'موبایل',
        'sms_price' => 'هزینه اس ام اس',
        'avatar' => 'آوتار'
    ],

];

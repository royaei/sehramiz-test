# Sehramiz

## Install

1. ```cd``` to localhost
2. ```git git@gitlab.com:magnait/Sehramiz.git```
3. Copy ```.env.example``` to root of project by ```.env``` name (Please do not rename it).
4. Create a database
5. Set configuration in ```.env``` file.
6. Run migrations and seed by ```php artisan migrate:refresh --seed``` (With ```sudo``` in linux)
7. Login with admin
    * url: ```./auth/admin/login```,
    * username: ```admin```
    * password: ```123456```
8. Enjoy :))

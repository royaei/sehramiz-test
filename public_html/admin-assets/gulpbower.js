var gulp         = require('gulp')
,   watch        = require('gulp-watch')
,   uglify       = require('gulp-uglify')
,   rename       = require('gulp-rename')
,   minifyCss    = require('gulp-minify-css');

var b = './bower_components';

var distJs = 'dist/js';
var distCss = 'dist/css';
var distFonts = distCss + '/../fonts';
var distImages = 'dist/images';
var i18n = 'fa';

gulp.task('bower', function() {

    // jQuery
    gulp.src(b + '/jquery/dist/jquery.min.js').pipe(gulp.dest(distJs));

    // jquery-ui
    gulp.src(b + '/jquery-ui/jquery-ui.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/jquery-ui/themes/base/jquery-ui.min.css').pipe(gulp.dest(distCss));

    // Bootstrap
    gulp.src(b + '/bootstrap/dist/js/bootstrap.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/bootstrap/dist/css/bootstrap.min.css').pipe(gulp.dest(distCss));
    gulp.src(b + '/bootstrap/dist/css/bootstrap.min.css.map').pipe(gulp.dest(distCss));
    gulp.src(b + '/bootstrap/dist/fonts/*.*').pipe(gulp.dest(distFonts));

    // codemirror
    gulp.src(b + '/codemirror/lib/codemirror.js').pipe(uglify()).pipe(gulp.dest(distJs));
    gulp.src(b + '/codemirror/lib/codemirror.css').pipe(minifyCss({keepSpecialComments: 0})).pipe(gulp.dest(distCss));
    gulp.src(b + '/codemirror/theme/base16-light.css').pipe(minifyCss({keepSpecialComments: 0})).pipe(gulp.dest(distCss));

    // InputMask
    gulp.src(b + '/jquery.inputmask/dist/jquery.inputmask.bundle.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/jquery.inputmask/dist/inputmask/jquery.inputmask.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/jquery.inputmask/dist/inputmask/jquery.inputmask.numeric.extensions.min.js').pipe(gulp.dest(distJs));

    // metisMenu
    gulp.src(b + '/metisMenu/dist/metisMenu.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/metisMenu/dist/metisMenu.min.css').pipe(gulp.dest(distCss));

    // icheck
    gulp.src(b + '/icheck/icheck.min.js').pipe(gulp.dest(distJs));

    // vex
    gulp.src(b + '/vex/js/vex.combined.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/vex/css/vex.css').pipe(minifyCss({keepSpecialComments: 0})).pipe(gulp.dest(distCss));
    gulp.src(b + '/vex/css/vex-theme-default.css').pipe(minifyCss({keepSpecialComments: 0})).pipe(gulp.dest(distCss));

    // font-awesome
    gulp.src(b + '/font-awesome/css/font-awesome.min.css').pipe(gulp.dest(distCss));
    gulp.src(b + '/font-awesome/fonts/*.*').pipe(gulp.dest(distFonts));

    // multiselect
    gulp.src(b + '/multiselect/js/jquery.multi-select.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/multiselect/css/multi-select.css').pipe(minifyCss({keepSpecialComments: 0})).pipe(gulp.dest(distCss));

    // parsleyjs
    gulp.src(b + '/parsleyjs/dist/parsley.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/parsleyjs/dist/i18n/' + i18n + '.js').pipe(uglify()).pipe(rename('parsley-i18n.' + i18n + '.min.js')).pipe(gulp.dest(distJs));

    // remarkable-bootstrap-notify
    gulp.src(b + '/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js').pipe(gulp.dest(distJs));

    // underscore
    gulp.src(b + '/underscore/underscore-min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/underscore/underscore-min.map').pipe(gulp.dest(distJs));

    // begard
    gulp.src(b + '/begard/dist/js/begard.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/begard/dist/css/themes/default/style.min.css').pipe(rename('begard.min.css')).pipe(gulp.dest(distCss));

    // tinymce
    gulp.src(b + '/tinymce/tinymce.min.js').pipe(gulp.dest(distJs + '/tinymce'));
    gulp.src(b + '/tinymce/plugins/*/*.*').pipe(gulp.dest(distJs + '/tinymce/plugins'));
    gulp.src(b + '/tinymce/skins/*/**/*.*').pipe(gulp.dest(distJs + '/tinymce/skins'));
    gulp.src(b + '/tinymce/themes/*/*.*').pipe(gulp.dest(distJs + '/tinymce/themes'));

    // devbridge-autocomplete
    gulp.src(b + '/devbridge-autocomplete/dist/jquery.autocomplete.min.js').pipe(gulp.dest(distJs));

    // angular
    gulp.src(b + '/angular/angular.min.js').pipe(gulp.dest(distJs));
    gulp.src(b + '/angular/angular.min.map').pipe(gulp.dest(distJs));

    // localforage
    gulp.src(b + '/localforage/dist/localforage.min.js').pipe(gulp.dest(distJs));

    // Chart.js
    gulp.src(b + '/Chart.js/dist/Chart.min.js').pipe(gulp.dest(distJs));
});

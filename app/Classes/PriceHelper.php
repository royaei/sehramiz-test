<?php

namespace Sehramiz\Classes;

class PriceHelper
{
    protected $price = 0;

    protected $inFa = false;

    protected $inToman = false;

    protected $inSep = false;

    protected $inInt = false;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function sep()
    {
        $this->inSep = true;

        return $this;
    }

    public function toman()
    {
        $this->inToman = true;

        return $this;
    }

    public function fa()
    {
        $this->inFa = true;

        return $this;
    }

    public function int($last = false)
    {
        $this->inInt = true;

        if ($last)
            return (int) $this->__toString();

        return $this;
    }

    public function format()
    {
        return $this->__toString();
    }

    public function __toString()
    {
        $price = $this->price;
        if ($this->inInt) {
            $price = intval(str_replace(',', '', $price));
        }

        if ($this->inToman) {
            $price /= 10;
        }

        if ($this->inSep) {
            $price = number_format($price, 0, '.', ',');
        }

        if ($this->inFa) {
            $price = fa($price);
        }

        return (string) $price;
    }

    public static function price($price)
    {
        return new static($price);
    }
}

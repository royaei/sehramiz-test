<?php

namespace Sehramiz\Classes;

/**
 * For models have timestamps
 */
trait EloquentGenderTrait
{
    /**
     * Payment status message
     *
     * @return string
     */
    public function getGender($plural = true, $unkown = null)
    {
        if (real_empty($this->gender)) {
            return is_null($unkown) ? trans('general-words.gender_either') : $unkown;
        }

        switch ($this->gender) {
            case C::MALE:
                return $plural ? trans('general-words.males') : trans('general-words.male');
                break;

            case C::FEMALE:
                return $plural ? trans('general-words.females') : trans('general-words.female');
                break;

            case C::GENDER_EITHER:
                return trans('general-words.gender_either');
                break;
        }

    }
}

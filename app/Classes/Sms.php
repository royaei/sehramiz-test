<?php

namespace Sehramiz\Classes;

use C;
use Date\Date;
use SoapClient;
use Sehramiz\Models\ReportSmsSend;

class Sms
{
    protected $url = 'http://sms.3300.ir/almassms.asmx?wsdl';

    protected $username = '';

    protected $password = '';

    protected $methodErrors = [
        0 => ['title' => 'اجرای سرویس با خطای غیر منتظره (Exception)مواجه شده است.لطفا پارامتر های خود را چک نمایید'],
        14 => ['title' => 'اعتبار ریالی کافی برای اجرای متد وجود ندارد.'],
        18 => ['title' => 'نام کاربری یا کلمه عبور اشتباه است'],
        20 => ['title' => 'شناسه پیامک اشتباه است'],
        100 => ['title' => 'طول آرایه ENCODING اشتباه است'],
        101 => ['title' => 'طول آرایه MESSAGE اشتباه است'],
        102 => ['title' => 'طول آرایه اشتباه MCLASS است'],
        103 => ['title' => 'طول آرایه MOBILES اشتباه است'],
        104 => ['title' => 'طول آرایه UDH اشتباه است'],
        1000 => ['title' => 'ارسال پیامک توسط شرکت موقتا قطع شده است'],
    ];

    protected $magfaErrors = [
        1   => ['title' => 'شماره گیرنده)recipientNumber ( نا معتبر)خالی( است'],
        2   => ['title' => 'شماره فرستنده )senderNumber ( نا معتبر)خالی( است'],
        3   => ['title' => 'پارامتر encoding نامعتبر است'],
        4   => ['title' => 'پارامتر messageClass نامعتبر است'],
        6   => ['title' => 'پارامتر udh نامعتبر است'],
        13  => ['title' => 'پیامک )ترکیب udh وmessageBody ( خالی است'],
        14  => ['title' => 'حساب اعتبار مورد نیاز را دارا نمی باشد'],
        15  => ['title' => 'سرور در هنگام ارسال پیام مشغول برطرف نمودن ایراد داخلی بوده است. پیام ها دوباره ارسال شوند'],
        16  => ['title' => 'حساب غیرفعال میباشد'],
        17  => ['title' => 'حساب منقضی شده است ) ) Expire'],
        18  => ['title' => 'نام کاربری و یا کلمه عبور نامعتبر است'],
        19  => ['title' => 'درخواست دارای اعتبار نمی باشد) ) Authentication failure'],
        20  => ['title' => 'شماره اختصاصی با نا کاربر حساب تطبیق ندارد'],
        22  => ['title' => 'استفاده از این سرویس برای این حساب مقدور نمی باشد'],
        23  => ['title' => 'به دلیل ترافیک باال، سرور آمادگی دریافت پیام جدید را ندارد. دوباره سعی کنید'],
        24  => ['title' => 'شناسه پیامک معتبر نمی باشد'],
        25  => ['title' => 'نوع سرویس درخواستی نامعتبر است'],
        27  => ['title' => 'شماره فرستنده در لیست غیرفعال شرکت همراه اول قراردارد(ارسال پیامک های تبلیغاتی برای این شماره امکان پذیر نیست)'],
        101 => ['title' => 'طول آرایه پارامتر messageBodies با طول آرایه گیرندگان تطابق ندارد'],
        102 => ['title' => 'طول آرایه پارامتر messageClassبا طول آرایه گیرندگان تطابق ندارد'],
        103 => ['title' => 'طول آرایه پارامتر senderNumber با طول آرایه گیرندگان تطابق ندارد'],
        104 => ['title' => 'طول آرایه پارامتر udhs با طول آرایه گیرندگان تطابق ندارد'],
        105 => ['title' => 'طول آرایه پارامتر priorities با طول آرایه گیرندگان تطابق ندارد'],
        106 => ['title' => 'آرایه گیرندگان خالی میباشد'],
        107 => ['title' => 'طول آرایه پارامتر گیرندگان بیشتر از طول مجاز است'],
        108 => ['title' => 'آرایه ی فرستندگان خالی می باشد'],
        109 => ['title' => 'طول پارامتر priorities با طول آرایه گیرندگان تطابق ندارد'],
        110 => ['title' => 'طول پارامتر checkingMessagelds با طول آرایه گیرندگان تطابق ندارد'],
    ];

    public function __construct()
    {

    }

    public function sendOne($mobile, $text, $jobId = null, $relatedCode = null, $relatedId = null)
    {
        return $this->send([$mobile], [$text], $jobId, $relatedCode, $relatedId);
    }

    public function send(array $mobiles, array $texts, $jobId = null, $relatedCode = null, $relatedId = null)
    {
        try {
            $soap = new SoapClient($this->url);

            $fields = array(
                'pUsername' => $this->username,
                'pPassword' => $this->password,
                'messages' => ['string' => $texts],
                'mobiles' => ['string' => $mobiles],
                'Encodings' => ['int' => 2],
                'mclass' => ['int' => 1],
            );

            $response = $soap->SendSms2($fields);

            if ($response->SendSms2Result < 0) { // Send Succeed

                $reports = [];

                if (is_array($response->pMessageIds->long)) {
                    for ($i = 0; $i < count($response->pMessageIds->long); $i++) {
                		if ($response->pMessageIds->long[$i] < 1000) {
                            $jobDesc = @$response->pMessageIds->long[$i]." - ".@$this->magfaErrors[$response->pMessageIds->long[$i]]['title'];
                            $reports[] = $this->createRowLog($jobId, $jobDesc, $texts[$i], $mobiles[$i], false, $relatedCode, $relatedId);
                        } else {
                            $reports[] = $this->createRowLog($jobId, null, $texts[$i], $mobiles[$i], true, $relatedCode, $relatedId);
                        }
                	}
                } else {
                    if ($response->pMessageIds->long <= 1000) {
                        $jobDesc = @$response->pMessageIds->long." - ".@$this->magfaErrors[$response->pMessageIds->long]['title'];
                        $reports[] = $this->createRowLog($jobId, $jobDesc, $texts[0], $mobiles[0], false, $relatedCode, $relatedId);
                    } else {
                        $reports[] = $this->createRowLog($jobId, null, $texts[0], $mobiles[0], true, $relatedCode, $relatedId);
                    }
                }

                ReportSmsSend::insert($reports);

            } else { // Send Failed
                $reports = [];
                for ($i = 0; $i < count($mobiles); $i++) {
                    $reports[] = $this->createRowLog($jobId, @$this->methodErrors[$response->SendSms2Result]['title'], $texts[$i], $mobiles[$i], false, $relatedCode, $relatedId);
                }

                ReportSmsSend::insert($reports);

                return false;
            }
        } catch(\Exception $e) {
            $reports = [];
            for ($i = 0; $i < count($mobiles); $i++) {
                $reports[] = $this->createRowLog($jobId, $e->getMessage(), $texts[$i], $mobiles[$i], false, $relatedCode, $relatedId);
            }

            ReportSmsSend::insert($reports);

            return false;
        }

        return true;
    }

    public function createRowLog($jobId, $jobDesc, $text, $mobile, $status, $relatedCode, $relatedId)
    {
        return [
            'job_id' => $jobId,
            'job_desc' => $jobDesc,
            'sms_text' => nl2br($text),
            'mobile' => $mobile,
            'status' => $status,
            'created_at' => Date::now(),
            'updated_at' => Date::now(),
            'related_code' => $relatedCode,
            'related_id' => $relatedId
        ];
    }
}

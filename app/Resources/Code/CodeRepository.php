<?php

namespace Sehramiz\Resources\Code;

use Auth;
use DateTime;
use Illuminate\Http\Request;
use Sehramiz\Models\FestivalCode;
use Sehramiz\Resources\Repository;
use Illuminate\Support\Facades\Lang;
use Illuminate\Database\DatabaseManager;

class CodeRepository extends Repository
{
    public function __construct
    (
        Request $request,
        DatabaseManager $db,
        FestivalCode $festivalCode
    )
    {
        $this->model = $festivalCode;
        $this->request = $request;
        $this->db = $db;
    }
}

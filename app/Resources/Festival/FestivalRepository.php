<?php

namespace Sehramiz\Resources\Festival;

use Auth;
use DateTime;
use Sehramiz\Models\Festival;
use Illuminate\Http\Request;
use Sehramiz\Resources\Repository;
use Illuminate\Support\Facades\Lang;
use Illuminate\Database\DatabaseManager;

class FestivalRepository extends Repository
{
    public function __construct
    (
        DatabaseManager $db,
        Festival $festival,
        Request $request
    )
    {
        $this->request = $request;
        $this->model = $festival;
        $this->db = $db;
    }

    /**
     * New festival from admin
     *
     * @return Sehramiz\Models\Festival
     */
    public function newFestival()
    {
        $this->beginTransaction();

        $festival = new $this->model;
        $festival->name = $this->request->get('name');
        $festival->codename = $this->request->get('codename');
        $festival->code_price = $this->request->get('code_price');
        $festival->status = checkStatus($this->request->get('status'));

        if (!real_empty($this->request->get('code_count_max'))) {
            $festival->code_count_max = $this->request->get('code_count_max');
        }

        return $festival;
    }

    /**
     * Update festival from admin
     *
     * @return Sehramiz\Models\Festival
     */
    public function update($festivalId)
    {
        $this->beginTransaction();

        $festival = $this->model->find($festivalId);
        $festival->name = $this->request->get('name');
        $festival->codename = $this->request->get('codename');
        $festival->code_price = price($this->request->get('code_price'))->int();
        $festival->status = checkStatus($this->request->get('status'));

        if (!real_empty($this->request->get('code_count_max'))) {
            $festival->code_count_max = $this->request->get('code_count_max');
        } else {
            $festival->code_count_max = null;
        }

        return $festival;
    }

    public function partners()
    {
        return is_array($this->request->get('partners')) ? $this->request->get('partners') : [];
    }

    /**
     * Transaction
     */
    public function transaction($festival, $partners = [])
    {
        try {
            $festival->save();
            $festival->partners()->sync($partners);

            $this->commit();
            return $festival;
        }
        catch(\PDOException $e) {
            $this->rollback();
            throw $e;
        }
    }
}

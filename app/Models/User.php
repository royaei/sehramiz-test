<?php

namespace Sehramiz\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Sehramiz\Classes\EloquentGenderTrait;
use Illuminate\Auth\Passwords\CanResetPassword;
use Sehramiz\Classes\EloquentTimestampJalaliTrait;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EloquentTimestampJalaliTrait, EloquentGenderTrait;

    protected $table = 'users';

    protected $primaryKey = 'user_id';

    protected $fillable = ['name', 'email', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function getFullNameAttribute()
    {
        return "$this->name $this->last_name";
    }
}

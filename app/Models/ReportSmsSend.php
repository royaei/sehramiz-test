<?php

namespace Sehramiz\Models;

use Illuminate\Database\Eloquent\Model;

class ReportSmsSend extends Model
{
    protected $table = 'report_sms_sends';

    protected $primaryKey = '';
}

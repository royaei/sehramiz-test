<?php

namespace Sehramiz\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            switch ($guard) {
                case 'admin':
                    return redirect(action('Admin\DashboardController@getIndex'));
                    break;

                case 'partner':
                    return redirect(action('Partner\DashboardController@getIndex'));
                    break;

                case 'front':
                    return redirect(action('Front\IndexController@getIndex'));
                    break;
            }
        }

        return $next($request);
    }
}

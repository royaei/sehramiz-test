<?php

namespace Sehramiz\Http\Controllers\Admin;

use Sehramiz\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    protected function fire($view, $data = array())
    {
        $extraData = array(
            'category' => $this->category
        );

        return view($view, array_merge($data, $extraData));
    }
}

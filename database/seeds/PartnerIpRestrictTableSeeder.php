<?php

use Illuminate\Database\Seeder;

class PartnerIpRestrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partnersIpRestrict = [
            array(
                'partner_id' => 1,
                'ip' => '127.0.0.1'
            )
        ];
        DB::table('partners_ip_restrict')->insert($partnersIpRestrict);
    }
}

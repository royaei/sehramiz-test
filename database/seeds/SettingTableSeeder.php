<?php

use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            array(
                'setting_id' => 1,
                'code' => 'sms_price',
                'value' => '160',
                'major' => 1
            )
        ];
        DB::table('settings')->insert($settings);
    }
}
